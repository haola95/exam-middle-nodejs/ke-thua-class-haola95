import { People } from './people.js';

// class con Motobike
class Worker extends People {
    constructor(paramHoTen, paramNgaySinh, paramQueQuan, paramNganhNghe, paramNoiLamViec, pamramLuong ) {
        super(paramHoTen, paramNgaySinh, paramQueQuan);
        
        this.NganhNghe = paramNganhNghe;
        this.NoiLamViec = paramNoiLamViec;
        this.Luong = pamramLuong;
    }
}

export { Worker };