import { Student } from './student.js';

// class con Motobike
class CollegeStudent extends Student {
    constructor(paramHoTen, paramNgaySinh, paramQueQuan, paramNameSchool, paramClass, paramMobile, paramChuyenNganh, paramMSSV) {
        super(paramHoTen, paramNgaySinh, paramQueQuan, paramNameSchool, paramClass, paramMobile);
        
        this.ChuyenNganh = paramChuyenNganh;
        this.MaSoSinhVien = paramMSSV;
        
    }
}

export { CollegeStudent };