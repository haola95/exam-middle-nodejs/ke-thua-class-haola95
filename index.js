import { People } from './people.js';
import { Student } from './student.js';
import { Worker } from './worker.js';
import { CollegeStudent } from './collegestudent.js';


var people_1 = new People("Anh Hao", 5, "Binh Duong");
console.log(people_1)
console.log("People 1 có Họ tên: " + people_1.HoTen);
console.log("People 1 có Quê Quán: " + people_1.QueQuan);

var worker_1 = new Worker ("Anh Hao", 5, "Binh Duong", "IT", "IRON", "1000$");
console.log(worker_1)
console.log("Worker 1 có Ho Ten: ", worker_1.HoTen);
console.log("Worker 1 có Ngày Sinh: ", worker_1.NgaySinh);
console.log("Worker 1 có Nơi Làm Việc: ", worker_1.NoiLamViec);
console.log("Worker 1 có Lương: ", worker_1.Luong);


var student_1 = new Student ("Anh Hao", 5, "Binh Duong", "IronHack", "J2223", "0945657412");
console.log(student_1)
console.log("Student 1 có Ho Ten: ", student_1.HoTen);
console.log("Student 1 có Ngày Sinh: ", student_1.NgaySinh);
console.log("Student 1 có Trường học: ", student_1.Class);
console.log("Student 1 có Sdt: ", student_1.Mobile);


var collegestudent_1 = new CollegeStudent ("Anh Hao", 5, "Binh Duong", "IronHack", "J2223", "0945657412", "IT", "015414");
console.log(collegestudent_1)
console.log("CollegeStudent 1 có Ho Ten: ", collegestudent_1.HoTen);
console.log("CollegeStudent 1 có Ngày Sinh: ", collegestudent_1.NgaySinh);
console.log("CollegeStudent 1 có Trường học: ", collegestudent_1.Class);
console.log("CollegeStudent 1 có Sdt: ", collegestudent_1.Mobile);
console.log("CollegeStudent 1 có Chuyên Ngành: ", collegestudent_1.ChuyenNganh);
console.log("CollegeStudent 1 có Mã số sinh viên: ", collegestudent_1.MaSoSinhVien);


// kiểm tra true false instanceof
console.log("Student 1 có nằm trong People ");
console.log(student_1 instanceof People);
console.log("CollegeStudent 1 có nằm trong People ");
console.log(collegestudent_1 instanceof People);
console.log("CollegeStudent 1 có nằm trong Worker ");
console.log(collegestudent_1 instanceof Worker);


