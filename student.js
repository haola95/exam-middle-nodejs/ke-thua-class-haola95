import { People } from './people.js';

// class con O to
class Student extends People {
    constructor(paramHoTen, paramNgaySinh, paramQueQuan, paramNameSchool, paramClass, paramMobile) {
        super(paramHoTen, paramNgaySinh, paramQueQuan);

        this.NameSchool = paramNameSchool;
        this.Class = paramClass;
        this.Mobile = paramMobile;
    }

}

export { Student };